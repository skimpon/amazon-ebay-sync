<?php
require 'vendor/autoload.php';
require_once '.sql-config.inc.php';

function getInventorySupplyList() {
    $amz = new AmazonInventoryList();
    $amz->setUseToken(); // GET ALL OF TOKENS
    $amz->setStartTime('-30 days'); // get inventory list from last month
    $amz->fetchInventoryList();
    return $amz->getSupply();
}

return getInventorySupplyList();
?>
