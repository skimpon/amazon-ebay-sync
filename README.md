# Amazon Ebay Sync

## Directions
1. Install composer packages. 
```
php composer.phar install
```
2. Add ebay credentials to **.ebay-config.inc.php.example** and rename it to **.ebay-config.inc.php**
3. Add amazon credentials to **.amazon-config.php.example**, rename it to **amazon-config.php**, and move this file to **/vendor/cpigroup/php-amazon-mws/**
4. Add database credentials to **.sql-config.inc.php.example** and rename it to **.sql-config.inc.php**
4. Make a **log.txt** file under **/vendor/cpigroup/php-amazon-mws/**
5. If using the scripts AS IS, use the provided database scheme provided in **ebay_amazon.sql**
**Note:** You might also want to fill out BOTH the sandbox and production keys under **.ebay-config.inc.php**