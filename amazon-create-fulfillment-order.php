<?php
require 'vendor/autoload.php';
require '.sql-config.inc.php';

function createAmazonOrder($orderDetails) {
    $amz = new AmazonFulfillmentOrderCreator(); //store name matches the array key in the config file
	$amz->setFulfillmentOrderId($orderDetails['order_id']); // matches ebay order_id
    $amz->setDisplayableOrderId($orderDetails['order_id']);
    $amz->setFulfillmentAction('Ship');
	$amz->setDate('now');
    $amz->setComment($orderDetails['displayable_order_comment']); // TODO: does there need to be a comment? could we set this from ebay?
    $amz->setShippingSpeed('Standard'); // TODO: standard, expedited, and priority are available
	$amz->setAddress($orderDetails['shipping_details']);
	$amz->setEmails($orderDetails['email']);
	$amz->setItems($orderDetails['item_orders']); //TODO: get all of the items from database, must returned as array
	return $amz->createOrder();
}

function retrieveOrderByID($order_id) {
    global $db;
    try {
        $query = $db->prepare('SELECT Name, Line1, Line2, City, StateOrProvinceCode, CountryCode, PhoneNumber, PostalCode, Email FROM ea_orders
                               WHERE order_id=?');
        $query->execute(array($order_id));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        return $row;
    } catch (PDOException $ex) {
        echo "Error occurred while trying to retrieve the order by id: ".$ex->getMessage();
    }
    return null;
}

function retrieveItemsByID($order_id) {
    global $db;
    try {
        $query = $db->prepare('SELECT * FROM transaction_orders WHERE order_id=?');
        $query->execute(array($order_id));
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        return $row;
    } catch (PDOException $ex) {
        echo "Error occurred while trying to retrieve list of transactions by order id: ".$ex->getMessage();
    }
}

function getOrderDetails($order_id) {
    $db_orderDetails = retrieveOrderByID($order_id);
    $db_transactions = retrieveItemsByID($order_id);
    $orderDetails = array(
        'order_id'                  => $order_id, // PULL FROM EBAY ORDER ID
        'displayable_order_comment' => 'n/a', // TODO: see if this should be pulled from ebay
        'shipping_details'          => array(
            'Name'              => $db_orderDetails['Name'],
            'Line1'             => $db_orderDetails['Line1'],
            'Line2'             => $db_orderDetails['Line2'],
            'City'              => $db_orderDetails['City'],
            'StateOrProvinceCode' => $db_orderDetails['StateOrProvinceCode'],
            'CountryCode'           =>$db_orderDetails['CountryCode'],
            'PostalCode'        => $db_orderDetails['PostalCode'],
            'PhoneNumber'       => $db_orderDetails['PhoneNumber'],
        ),
        'email'                     => array($db_orderDetails['Email']), //optional TODO: DO YOU WANT TO SPECIFICALLY SET EMAIL FOR THIS SHIPMENT?
        'item_orders'               => $db_transactions
    );
    return $orderDetails;
}

function getPendingOrders() { // TODO: could clean this up by making a helper function
    global $db;                // to grab order ids based on ea_status
    try {
        $query = $db->prepare('SELECT order_id FROM ea_orders WHERE ea_status=?');
        $query->execute(array("PENDING"));
        $row = $query->fetchAll(PDO::FETCH_COLUMN);
        return $row;
    } catch (PDOException $ex) {
        echo "Error while trying to get orders from database that is marked as 'PENDING: '".$ex->getMessage();
    }
}

function markOrderCreated($order_id) {
    global $db;
    try {
        $query = $db->prepare('UPDATE ea_orders SET ea_status=? WHERE order_id=?');
        $query->execute(array("ORDER_CREATED", $order_id));
        // should make sure affected rows == 1
        // debugging: $affected_rows = $query->rowCount();
    } catch (PDOException $ex) {
        echo "Error while trying to mark order as 'ORDER_CREATED': ".$ex->getMessage();
    }
}

// Actual script: for every order that is ea_status === "PENDING",
// make an order.
$pendingOrders = getPendingOrders();
foreach($pendingOrders as $order_id) {
    $orderDetails = getOrderDetails($order_id);
    $successfulOrder = createAmazonOrder($orderDetails);
    echo "order_id: " . $order_id . "," . $successfulOrder;
    if($successfulOrder) {
        markOrderCreated($order_id);
    }
    /*else { // TODO: should log error into database if there was an error
    }*/
    echo '<br><br>';
}
