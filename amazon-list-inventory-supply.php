<?php
require_once 'vendor/autoload.php';
require_once '.sql-config.inc.php';
    // link = amazon-list-inventory-supply.php?page=1&limit=50
    if ($_GET['page'] == 0) {
        $inventory_supply = include_once('get-amazon-list-inventory-supply.php');
        file_put_contents('cache/inventory-supply.txt', serialize($inventory_supply));
    } else {
        $inventory_supply = unserialize(file_get_contents('cache/inventory-supply.txt'));
    }
    $page = $_GET['page'] ? $_GET['page'] : 0;
    $limit = $_GET['limit'];
    $rows = array_chunk($inventory_supply,$limit);
    $result = $rows[$page];

function getTitleFromSKU_db($sellerSKU) {
    global $db;
    try {
        $stmt = $db->prepare('Select Title FROM product_list WHERE SellerSKU=?');
        $stmt->execute(array($sellerSKU));
        return $stmt->fetchColumn();
    } catch (PDOException $ex) {
        echo "Could not get title from SKU: " + $ex->getMessage();
    }
}

class ProductInfoGetter extends AmazonProduct {
    public function getTitle($productInstance) {
        $productDetails = $productInstance->getData();
        $title = $productDetails['AttributeSets'][0]['Title'];
        return $title;
    }
}

function getProductFromASIN_amz($asin) {
    $amz = new AmazonProductList();
    $amz->setIdType('ASIN');
    $amz->setProductIds($asin);
    $amz->fetchProductList();
    return $amz->getProduct()[0];
}

function addProductListItem($sku, $title) {
    global $db;
    try {
        $query = $db->prepare('INSERT product_list(SellerSKU, Title)
                    VALUES (?, ?)');
        $query->execute(array($sku, $title));
    } catch(PDOException $ex){
        echo "Could not add product to product_list: " + $ex->getMessage();
    }
}

function getProductTitle($sku, $asin) {
    $title = getTitleFromSKU_db($sku);
    if ($title) {
        return $title;
    } else {
        $product = getProductFromASIN_amz($asin);
        $productInfoGetter = new ProductInfoGetter();
        $productTitle = $productInfoGetter->getTitle($product);
        addProductListItem($sku, $productTitle);
        return $title;
    }
}
?>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amazon List Inventory Supply</title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>
<body>
<style>
    .content-wrapper {
        margin: 20px;
    }
</style>
<div class="content-wrapper">
    <div class="content">

<h1>Inventory Supply List</h1>
 <?php
    if($page>0)
    {
        ?>
        <a href="amazon-list-inventory-supply.php?limit=<?php echo $limit; ?>&page=<?php echo ($page-1); ?>">Previous</a>
        <?php
    }
    ?>
    <?php
    if (isset($rows[$page+1]))
    {
        ?>
        <a href="amazon-list-inventory-supply.php?limit=<?php echo $limit; ?>&page=<?php echo ($page+1); ?>">Next</a>
        <?php
    }
    ?>
<table id="table_products" class="pure-table pure-table-bordered">
<thead>
<tr>
    <th>Title</th>
    <th>SellerSKU</th>
    <th>ASIN</th>
    <th>Tot.Qty.</th>
    <th>FNSKU</th>
    <th>Condition</th>
    <th>InStockQty.</th>
    <th>Avail.</th>
</tr>
</thead>
    <?php
        foreach ($result as $row) {
            echo "<tr>";
            echo "<td class='product-title'>".getProductTitle($row['SellerSKU'], $row['ASIN'])."</td>";
            foreach ($row as $column) {
                echo "<td>$column</td>";
            }
            echo "</tr>";
        }
    ?>

</table>
        <?php
            if($page>0)
            {
                ?>
                <a href="amazon-list-inventory-supply.php?limit=<?php echo $limit; ?>&page=<?php echo ($page-1); ?>">Previous</a>
                <?php
            }
            ?>
            <?php
            if (isset($rows[$page+1]))
            {
                ?>
                <a href="amazon-list-inventory-supply.php?limit=<?php echo $limit; ?>&page=<?php echo ($page+1); ?>">Next</a>
                <?php
            }
            ?>
    </div>
</div>
</body>
</html>
