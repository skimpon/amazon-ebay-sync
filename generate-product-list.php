<?php
require 'vendor/autoload.php';
require '.sql-config.inc.php';

$inventory_list_unformatted_file = 'cache\\inventory_list_unformatted.txt';
$inventory_list_file = 'cache\\inventory_list.csv';
$inventory_list_unformatted_path= __DIR__ . '\\' . $inventory_list_unformatted_file;
$inventory_list_path = __DIR__ . '\\' . $inventory_list_file;

function getAmazonInventorySupply() {
    $amz = new AmazonReportRequestList();
    $amz->setReportTypes('_GET_MERCHANT_LISTINGS_ALL_DATA_');
    $amz->setReportStatuses('_DONE_');
    $amz->fetchRequestList();
    return $amz->getReportId();
}

function runRequestReport() {
    $amz = new AmazonReportRequest();
    $amz->setReportType('_GET_MERCHANT_LISTINGS_ALL_DATA_');
    $amz->setShowSalesChannel(false);
    $amz->requestReport();
    return $amz->getResponse();
}

function saveReport($reportId, $inventory_list_path_unformatted) {
    $amz = new AmazonReport();
    $amz->setReportId($reportId);
    $amz->fetchReport();
    $amz->saveReport($inventory_list_path_unformatted); // used later in formatReport
}

function formatReport($inventory_list_unformatted_path, $inventory_list_path) {
    $handle_r = fopen($inventory_list_unformatted_path, "r") or die ('Cannot open ' . $inventory_list_unformatted_path . ' for reading.');
    $handle_w = fopen($inventory_list_path, 'w') or die ('Cannot open ' . $inventory_list_path . ' for writing.');
    while (!feof($handle_r)) {
        $line = fgetcsv($handle_r, 0, "\t");
        $formattedLine = array($line[0], $line[3]);
        fputcsv($handle_w, $formattedLine);
    }
    fclose($handle_r);
    fclose($handle_w);
}

function saveProductList($inventory_list_path) {
    global $db;
    global $file_path;
    try {
        $query = $db->exec("LOAD DATA LOCAL INFILE '" .addslashes($inventory_list_path)."'
            INTO TABLE product_list
            FIELDS TERMINATED BY ','
            OPTIONALLY ENCLOSED BY '\"' 
            LINES TERMINATED BY '\n'
            IGNORE 1 LINES
            (Title, SellerSKU)");
    } catch(PDOException $ex) {
        echo "Could not insert product data into product_list table because: " . $ex->getMessage();
    }
}

runRequestReport();
echo "Waiting for report to generate...\n";
sleep(15);
$reportId = getAmazonInventorySupply();
echo "Saving to file...\n";
saveReport($reportId, $inventory_list_unformatted_path); // get raw report and save as unformatted file
formatReport($inventory_list_unformatted_path, $inventory_list_path); // save SellerSKU, Product Title to .csv file
saveProductList($inventory_list_path);
echo "Saving to database...\n";
