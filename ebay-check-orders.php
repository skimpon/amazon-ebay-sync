<?php
require_once 'vendor/autoload.php';
require_once '.sql-config.inc.php';

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;


$config = require '.ebay-config.inc.php';
$service = new Services\TradingService([
    'credentials' => $config['sandbox']['credentials'],
    'siteId'      => Constants\SiteIds::US,
    'sandbox'     => true,
	]);

function getRecentOrders(){
    global $config, $service;
    $request = new Types\GetOrdersRequestType();
    $request->NumberOfDays = 1; // TODO: Might need to use pagination if there are a lot of orders within this period.
    $request->OrderRole = "Seller"; // TODO: Should change NumberOfDays depending on frequency of cron job.
    $request->OrderStatus = "Completed";

    // use user token
    $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
    $request->RequesterCredentials->eBayAuthToken = $config['sandbox']['authToken'];

    // send request
    $response = $service->getOrders($request); 
    return $response;
}

/*
function outputObject($response) {
    // output
    if (isset($response->Errors)) {
	foreach ($response->Errors as $error) {
	    printf(
		"%s: %s\n%s\n\n",
		$error->SeverityCode === Enums\SeverityCodeType::C_ERROR
		? 'Error' : 'Warning',
		$error->ShortMessage,
		$error->LongMessage);
	}
    }
    return $response;
}
*/

function addEbayOrder($order_details) {
    global $db;
    // TODO: check each individual variable
    // and better error messages
    try {
        $query = $db->prepare("INSERT ea_orders(order_id, ebay_order_status, Name, Line1,
                        Line2, City, StateOrProvinceCode, CountryCode, CountryName,
                        PhoneNumber, PostalCode)
                                VALUES(:order_id, :ebay_order_status, :Name, :Line1,
                        :Line2, :City, :StateOrProvinceCode, :CountryCode, :CountryName,
                        :PhoneNumber, :PostalCode)");
        $query->execute(array(':order_id'		=> $order_details[0],
                    ':ebay_order_status'    	=> $order_details[1],
                    ':Name'	             		=> $order_details[2],
                    ':Line1'	                => $order_details[3],
                    ':Line2'        	        => $order_details[4],
                    ':City'	            		=> $order_details[5],
                    ':StateOrProvinceCode'          => $order_details[6],
                    ':CountryCode'	            => $order_details[7],
                    ':CountryName'      		=> $order_details[8],
                    ':PhoneNumber'		        => $order_details[9],
                    ':PostalCode'	        	=> $order_details[10]));

    } catch (PDOException $ex) {
        echo "An error occurred while adding the ebay order";
        echo $ex->getMessage();
	    // some_logging_function($ex->getMessage());
    }
}

function orderInTable($order_id) {
    global $db;
    try {
        $query = $db->prepare('SELECT * FROM ea_orders WHERE order_id=?');
        $query->execute(array($order_id));
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        if($result) {
            return true;
        }
        return false;

    } catch(PDOException $ex) {
        echo "Could not check for order in table";
    }
    return false;
}

function addTransaction($transaction_details) {
    global $db;
    try {
	$query = $db->prepare("INSERT transaction_orders(order_id, SellerSKU, Quantity, SellerFulfillmentOrderItemId, eBayItemId)
				VALUES (:order_id, :SellerSKU, :Quantity, :SellerFulfillmentOrderItemId, :eBayItemId)");
	$query->execute(array(':order_id'		                => $transaction_details[0],
				            ':SellerSKU'			        => $transaction_details[1],
				            ':Quantity'                     => $transaction_details[2],
                            ':SellerFulfillmentOrderItemId' => $transaction_details[3],
                            ':eBayItemId'                   => $transaction_details[4]));
    } catch (PDOException $ex) {
	    echo 'Error occurred trying to make a transaction.';
	    // do some php asserts here
	    // som_logging_function($ex->getMetMessage());
    }
}

function createOrderDetails($order) {
    $order_id = $order->OrderID;
    $ebay_order_status = $order->OrderStatus;

    $shippingAddress = $order->ShippingAddress;
    $name = $shippingAddress->Name;
    $address_line_1= $shippingAddress->Street1;
    $address_line_2 = $shippingAddress->Street2;
    $city = $shippingAddress->CityName;
    $state = $shippingAddress->StateOrProvince;
    $country = $shippingAddress->Country;
    $country_name = $shippingAddress->CountryName;
    $phone = $shippingAddress->Phone;
    $postal_code = $shippingAddress->PostalCode;

    echo "<br><b>Order ID: </b>".$order_id;
    echo "<br><b>ebay order status: </b>".$ebay_order_status;

    echo "<br><b>shipping address</b>";
    echo "<b><br>name: </b>".$name;
    echo "<b><br>address_line_1: </b>".$address_line_1;
    echo "<b><br>address_line_2: </b>".$address_line_2;
    echo "<b><br>city: </b>".$city;
    echo "<b><br>state: </b>".$state;
    echo "<b><br>country: </b>".$country;
    echo "<b><br>country_name: </b>".$country_name;
    echo "<b><br>phone: </b>".$phone;
    echo "<b><br>postal_code: </b>".$postal_code;
    echo "<br>";

    $orderDetails = array(
	$order_id,
	$ebay_order_status,
	$name,
	$address_line_1,
	$address_line_2,
	$city,
	$state,
	$country,
	$country_name,
	$phone,
	$postal_code,
	$ebay_order_status);


    return $orderDetails;
}

function createTransactionDetails($order_id, $transaction) {
    $sku = $transaction['Item']['SKU'];
    $quantity_purchased = $transaction['QuantityPurchased'];
    $seller_fulfillment_order_item_id = $transaction['OrderLineItemID'];
    $ebay_item_id = $transaction['Item']['ItemID'];

    $transaction_details = array($order_id, $sku, $quantity_purchased, $seller_fulfillment_order_item_id, $ebay_item_id);
    echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>sku: </b>".$sku;
    echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>quantity_purchased: </b>".$quantity_purchased;
    echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>order line item id: </b>".$seller_fulfillment_order_item_id;
    echo "<br>&nbsp;&nbsp;&nbsp;&nbsp;<b>item id: </b>".$ebay_item_id;
    echo "<br>----------------------<br><br>";
    return $transaction_details;
}

/* Actual script checks for eBay order and adds all the information
    to the local database under ea_orders and transaction_orders */
$recentOrders = getRecentOrders();
print_r($recentOrders);
/*
foreach($recentOrders->OrderArray->Order as $order) {
    $order_id = $order->OrderID;
    $ebay_order_status = $order->OrderStatus;
    $shipped = $order->ShippedTime;
    if($shipped) { // TODO: check for falsey variable issues
        $existingOrder = orderInTable($order_id);
        $orderDetails = createOrderDetails($order);
        addEbayOrder($orderDetails); // TODO: consider making this an "INSERT and/or UPDATE" function
        if(!$existingOrder) {
            $transactions = $order->TransactionArray->toArray();
            $transactions = $transactions['Transaction'];
            foreach ($transactions as $transaction) {

                var_dump($transaction);
                $transaction_details = createTransactionDetails($order_id, $transaction);
                addTransaction($transaction_details);
            }
        }
    }
}
*/
