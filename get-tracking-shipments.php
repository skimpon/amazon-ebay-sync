<?php
require 'vendor/autoload.php';
require '.sql-config.inc.php';

/**
 * The namespaces provided by the SDK.
 */
use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

$config = require '.ebay-config.inc.php';

$service = new Services\TradingService([
    'credentials' => $config['sandbox']['credentials'],
    'siteId'      => Constants\SiteIds::US,
    'sandbox'     => true
]);

// Orders in ea_orders that are marked with
// ea_status = "ORDER_CREATED" need to receive tracking information
function getOrderCreatedOrders() { // TODO: could clean this up by making a helper function to retrieve
    global $db;                                    // order ids based on ea_status
    try {
        $query = $db->prepare('SELECT order_id FROM ea_orders WHERE ea_status=?');
        $query->execute(array("ORDER_CREATED"));
        $row = $query->fetchAll(PDO::FETCH_COLUMN);
        return $row;
    } catch (PDOException $ex) {
        echo "Error while trying to get orders from database that is marked as 'ORDER_CREATED': ".$ex->getMessage();
    }
}

function getShippingDetails($order_id) {
    $amz = new AmazonFulfillmentOrder();
    $amz->setOrderId($order_id);
    $amz->fetchOrder();
    $order = $amz->getOrder();
    return $order['Items']; // should be return $order['Shipments'];
}

function updateDbTracking($order_item_id, $tracking_no, $carrier_code) {
    global $db;
    try {
        $query = $db->prepare('UPDATE transaction_orders SET tracking_no=?, carrier_code=? WHERE SellerFulfillmentOrderItemId=?');
        $query->execute(array($tracking_no, $carrier_code, $order_item_id));
    } catch (PDOException $ex) {
        echo "Updating tracking numbers: error occurred trying to update tracking_no field in transaction_orders: ".$ex->getMessage();
    }
}

function needsTrackingNo($order_item_id) {
    global $db;
    try {
        $query = $db->prepare('SELECT tracking_no FROM transaction_orders WHERE SellerFulfillmentOrderItemId=?');
        $query->execute(array($order_item_id));
        $row = $query->fetch(PDO::FETCH_ASSOC);
        if ($row['tracking_no']) {
            return false;
        } else {
            return true;
        }
    } catch (PDOException $ex) {
        echo "Error occurred while checking to see if tracking number exists: ".$ex->getMessage();
    }
}

function markAsSentTracking($order_id, $countShipped) {
    global $db;
    try {
        $query = $db->prepare('SELECT tracking_no FROM transaction_orders WHERE order_id=?');
        $query->execute(array($order_id));
        $row = $query->fetchAll(PDO::FETCH_ASSOC);
        foreach ($row as $package) {
            if (empty($package['tracking_no'])) {
                return;
            } else {
                $query = $db->prepare('UPDATE ea_orders SET ea_status=? WHERE order_id=?');
                $query->execute(array("SENT_TRACKING", $order_id));
            }
        }
    } catch (PDOException $ex) {
        echo "Error occurred while trying to mark order_id as 'SENT_TRACKING': ".$ex->getMessage();
    }
}

function sendTrackingNoToEbay($order_item_id, $shipmentDetails) {
    global $config, $service;
    $shipment = new Types\ShipmentType();
    $shipment ->ShipmentTrackingDetails[] = new \DTS\eBaySDK\Trading\Types\ShipmentTrackingDetailsType($shipmentDetails);
    $completeSaleRequest = new \DTS\eBaySDK\Trading\Types\CompleteSaleRequestType();
    $completeSaleRequest->OrderLineItemID = $order_item_id;
    $completeSaleRequest->Shipment = $shipment;

    $completeSaleRequest->RequesterCredentials = new Types\CustomSecurityHeaderType();
    $completeSaleRequest->RequesterCredentials->eBayAuthToken = $config['sandbox']['authToken'];
    return $completeSaleResponse = $service->completeSale($completeSaleRequest);
}

$orderCreatedOrders = getOrderCreatedOrders(); // TODO: for use in production
//$orderCreatedOrders = array("291312010"); // TODO: remove after testing
$testArrayDetails = array(          // TODO: remove after testing
    array(
        'FulfillmentShipmentStatus'         => "SHIPPED",
        'FulfillmentShipmentItem'           => array(
            'SellerSKU'                     => 'RD-FH4B-XMPF',
            'SellerFulfillmentOrderItemId'  => '110186000345-28247677001'
        ),
        'FulfillmentShipmentPackage'   => array(
            'TrackingNumber'           => '93ZZ00',
            'CarrierCode'              => 'UPS'
        )
    ),
    array(
        'FulfillmentShipmentStatus'    => "SHIPPED",
        'FulfillmentShipmentItem'      => array(
            'SellerSKU'                     => '04-54VO-1OLH',
            'SellerFulfillmentOrderItemId'  => '110186018863-28247688001'
        ),
        'FulfillmentShipmentPackage'   => array(
            'TrackingNumber'           => 'bitememother',
            'CarrierCode'              => 'UPS'
        )
    ),
);

foreach($orderCreatedOrders as $order_id) {
    $shippingDetails = getShippingDetails($order_id); // call to Amazon for shipping details
    //$shippingDetails = $testArrayDetails; // TODO: remove this after testing
    $countShipped = 0;
    foreach($shippingDetails as $shipment) {
        if ($shipment['FulfillmentShipmentStatus'] == 'SHIPPED') {
            $countShipped++;
            $order_item_id = $shipment['FulfillmentShipmentItem']['SellerFulfillmentOrderItemId'];
            if (needsTrackingNo($order_item_id)) {
                $tracking_no = $shipment['FulfillmentShipmentPackage']['TrackingNumber'];
                $carrier_code = $shipment['FulfillmentShipmentPackage']['CarrierCode'];
                updateDbTracking($order_item_id, $tracking_no, $carrier_code);
                $sendTracking = sendTrackingNoToEbay($order_item_id,
                    array(
                        "ShipmentTrackingNumber" => $tracking_no,
                        "ShippingCarrierUsed" => $carrier_code,     // TODO: should make gatebay call to make sure code is correct
                    )
                ); // update ebay tracking
                //var_dump($sendTracking); // TODO: should make function to print error messages; remove after testing
                echo $order_item_id . ": " . $tracking_no;
            }
            echo "<br><br>";
        }
    }
    markAsSentTracking($order_id, $countShipped); // Only mark as SENT_TRACKING if all packages are sent
}