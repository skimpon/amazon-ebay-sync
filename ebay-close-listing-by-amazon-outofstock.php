<?php
require 'vendor/autoload.php';
require '.sql-config.inc.php';

use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;

$config = require '.ebay-config.inc.php';
$service = new Services\TradingService([
    'credentials' => $config['sandbox']['credentials'],
    'siteId'      => Constants\SiteIds::US,
    'sandbox'     => true,
    ]);

function endEbayItem($item_sku) {
    global $config, $service;
    $request = new Types\EndFixedPriceItemRequestType();
    $request->RequesterCredentials = new Types\CustomSecurityHeaderType();
    $request->RequesterCredentials->eBayAuthToken = $config['sandbox']['authToken'];
    $request->SKU = $item_sku;// TODO: fix after testing
    $request->EndingReason = Enums\EndReasonCodeType::C_NOT_AVAILABLE;
    $response = $service->endFixedPriceItem($request);
    ebayPrintErrors($response);
}

function ebayPrintErrors($response) {
    if (isset($response->Errors)) {
        foreach ($response->Errors as $error) {
            printf(
                "%s: %s\n%s\n\n",
                $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning',
                $error->ShortMessage,
                $error->LongMessage
            );
        }
    }
    if ($response->Ack !== 'Failure') {
        printf("The item was ended at: %s\n", $response->EndTime->format('H:i (\G\M\T) \o\n l jS F Y'));
    }
}

function getRecentAmazonOrderItems() {
    $amz = new AmazonOrderList();
    $amz->setUseToken(); // GET ALL ORDER ITEMS
    //$amz->setFulfillmentChannelFilter(""); can set to AFN or MFN
    $amz->setLimits("Created", $lower = "-1 day"); // CAN BE CHANGED TO DO -1 hour, -5 minutes, etc...
    $amz->fetchOrders();
    return $amz->fetchItems();
}

function getOutofstockInventoryFromSKU($sellerSKUList) {
    $amz = new AmazonInventoryList();
    $amz->setUseToken();
    $amz->setSellerSkus($sellerSKUList);
    $amz->fetchInventoryList();
    return $amz->getSupply();
}

// 1. Get SKU list of recent orders from Amazon -- you can adjust how far back you want to go in this function!
$orderItems = getRecentAmazonOrderItems();
$skuList = array_map( function($item) {return $item->getSellerSKU(); }, $orderItems );
//print_r($skuList);

// 2. Get current stock from all the items in the skuList
// Then filter for empty SKUs.
$skuListDetails = getOutofstockInventoryFromSKU($skuList);
$skuOutofstockList = array_column( array_filter( $skuListDetails, function($item) {return $item['InStockSupplyQuantity'] == 0;}), 'SellerSKU' );
print_r($skuOutofstockList);

// 4. For every outofstock SKU, end the ebay item.
// If so, call EndFixedPriceItem on it.
foreach ($skuOutofstockList as $sku) {
//    endEbayItem($sku); // UNCOMMENT after successful testing.
}
