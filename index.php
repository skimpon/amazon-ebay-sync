<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Index</title>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
</head>
<body>
<style>
    .content-wrapper {
        padding: 20px;
    }
</style>
<div class="content-wrapper">
    <div class="content">

        <h1>Directory</h1>
        <div class="pure-g">
            <div class="pure-u-1-2">
                <h2 class="is-center">Scripts</h2>
                <ul>
                    <li><a href="generate-product-list.php">Generate Product List</a>: Prints list of product titles for use in Amazon-List-Inventory-Supply page.
                        Does not need to be run often.</li>
                    <li><a href="amazon-create-fulfillment-order.php">Amazon Create Fulfillment Order</a>: Sends unsent eBay Orders to Amazon.</li>
                    <li><a href="ebay-check-orders.php">Ebay Check Orders</a>: Script to check completed orders on eBay and save them to local database.</li>
                    <li><a href="get-tracking-shipments.php">Get Tracking Shipments</a>: Checks Amazon for tracking on shipments from local database.                        Sends tracking to eBay if any are available.</li>
                    <li><a href="ebay-close-listing-by-amazon-outofstock.php">Close eBay Listings on Amazon outofstock</a>: Check for recent Amazon orders, and close ebay listings for items that are out of stock.</li>
                </ul>
            </div>
            <div class="pure-u-1-2">
                <h2>Pages</h2>
                <ul>
                    <li><a href="amazon-list-inventory-supply.php?page=0&limit=50">Amazon List Inventory Supply</a>:
                        Must include page=?&limit=? parameters where page is the page # and limit is the # of inventory to
                        load at a time. May take a while to generate results for the first page.</li>
                </ul>
            </div>


        </div>
    </div>
</div>


</body>
</html>
